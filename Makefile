.PHONY: all
RELEASE_DIR = build
USR_DIR = /usr
OPT_DIR = $(RELEASE_DIR)/opt/Loc-OS-LPKG/glpkg
BIN_DIR = $(RELEASE_DIR)$(USR_DIR)/bin
DESKTOP_DIR = $(RELEASE_DIR)$(USR_DIR)/share/applications
BASH_DIR = $(RELEASE_DIR)$(USR_DIR)/share/bash-completion/completions
POLKIT_DIR = $(RELEASE_DIR)$(USR_DIR)/share/polkit-1/actions
LOCALE_DIR = $(RELEASE_DIR)$(USR_DIR)/share/locale
MIME_DIR = $(RELEASE_DIR)$(USR_DIR)/share/mime/packages

all: build

build:
	mkdir -p $(RELEASE_DIR) $(OPT_DIR) $(DESKTOP_DIR) $(POLKIT_DIR) $(BIN_DIR) $(MIME_DIR) $(BASH_DIR)
	./genlocale.sh
	cp -r glpkg glpkg.css libglpkg assets/icon-lpkg.png $(OPT_DIR)
	rm -rf $(OPT_DIR)/libglpkg/__pycache__ $(OPT_DIR)/__pycache__
	cp assets/org.loc-os.glpkg.policy $(POLKIT_DIR)
	cp assets/lpkg.xml $(MIME_DIR)
	cp assets/glpkg.desktop $(DESKTOP_DIR) 
	cp -r locale $(LOCALE_DIR)
	cp -r completions/bash/* $(BASH_DIR)
	ln -s /opt/Loc-OS-LPKG/glpkg/glpkg $(BIN_DIR)/glpkg

clean:
	rm -rf $(RELEASE_DIR) locale libglpkg/__pycache__ __pycache__


install:
	cp -rv build/* /

uninstall:
	rm -rf $(OPT_DIR) $(BIN_DIR)/glpkg $(LOCALE_DIR)/*/LC_MESSAGES/glpkg.mo $(DESKTOP_DIR)/glpkg.desktop $(POLKIT_DIR)/org.loc-os.glpkg.policy $(MIME_DIR)/lpkg.xml
