#!/bin/bash

mkdir -p /tmp/.LPKG
rm -f /tmp/.LPKG/*
if ! [ -f /opt/Loc-OS-LPKG/installed-lpkg/LinksLpkgRepo.list ]; then
    touch /opt/Loc-OS-LPKG/installed-lpkg/LinksLpkgRepo.list
fi

case $1 in
    install)
        /opt/Loc-OS-LPKG/Start_Install_Lpkg_localGUI.sh "$2" ;;
    remove)
        NameLPKG=$(echo "$2" |sed 's/[/]/ /g' | sed -e 's/.lpkg$//' |awk '{ print $NF}')
        echo "NameLpkg<-> $NameLPKG" > /tmp/.LPKG/RemoveLpkg.info
        echo "Remove LPKG" > /tmp/.LPKG/Button.info
        /opt/Loc-OS-LPKG/Info_RemoveLpkg ;;
    list)
        /usr/sbin/lpkg -i | grep "$2" -w ;;
    recreate)
        /usr/sbin/lpkg recreate-list;;
    image)
        if ! [ -f /opt/Loc-OS-LPKG/installed-lpkg/Listinstalled-lpkg.list ]; then
            touch /opt/Loc-OS-LPKG/installed-lpkg/Listinstalled-lpkg.list
        fi
        IMG_LPKG=$(tail /opt/Loc-OS-LPKG/installed-lpkg/Listinstalled-lpkg.list | cut -d\	 -f2 | grep "$2" | cut -d\  -f1 )
        if [ -f "$IMG_LPKG" ]; then
            echo "$IMG_LPKG"
        else
            echo "null"
        fi
        ;;
    *)
        echo "Error: glpkg-lib" ;;
esac
