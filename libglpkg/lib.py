#!/bin/python3
import os, subprocess, tarfile, logging
from optparse import OptionParser

parser = OptionParser()
parser.set_usage("%prog [options] [/path/to/file]")
parser.add_option("-t", "--timeout",
                    dest="timeout", default="3", type="string",
                    help="set timeout to check if a lpkgindex page exists in seconds (default is 3)")
parser.add_option("-v", "--verbose",
                    action="store_true", dest="verbose", default=False,
                    help="print debug messages")
parser.add_option("-d", "--debug",
                    action="store_true", dest="debug", default=False,
                    help="run in debug environment")
(options, lpkg) = parser.parse_args()
if options.debug:
    sh = "./libglpkg/glpkg-lib.sh"
else:
    sh = "/opt/Loc-OS-LPKG/glpkg/libglpkg/glpkg-lib.sh"

class LibGlpkg():
    """Functions for Glpkg"""
    def __init__(self):
        self.words = ["mantenedor", "paquete", "version", "licencia", "homepage"]
        self.name = ""
        self.pkgdesc = {}
        self.lpkg = lpkg[0]
        self.icon_lpkg = "/opt/Loc-OS-LPKG/glpkg/icon-lpkg.png"
    def xname(self, arg):
        """Extract name of package from args"""
        if arg != "":
            extracted = arg.split("/")[-1].split(".lpkg")[0].split("-")
            self.name = extracted[0]
        Log().debug(f"name: {self.name}")
        return self.name

    def rpkgdesc(self, pkg):
        """Read pkgdesc from LPKG file"""
        global lpkg
        info_pkg = ["null", "0", "0"]
        try:
            lpkg_file = tarfile.open(pkg, "r")
            Log().debug("Extracting pkgdesc...")
            pkgdesc_file = lpkg_file.extractfile("./desc/pkgdesc").readlines()
            
            for i in pkgdesc_file:
                for w in self.words:
                    if w in i.decode('utf-8'):
                        l = i.decode('utf-8')
                        if "'" in l:
                            self.pkgdesc.update({w: l.split("'")[1]})
                        elif "\"" in l:
                            self.pkgdesc.update({w: l.split("\"")[1]})
            Log().debug(f"pkgdesc: {self.pkgdesc}")
            return self.pkgdesc
        except:
            Log().error("Error while reading pkgdesc or not exists")
            try:
                if ".lpkg" in self.lpkg:
                    Log().warning("Getting some info from filename")
                    info_pkg = pkg.split("/")[-1].split(".lpkg")[0].split("-")
                    if len(info_pkg) < 3:
                        Log().warning("Filename is incomplete. Completing with null values")
                    while len(info_pkg) < 3:
                        info_pkg.append("null")
            except:
                Log().warning("Returning null values")
                info_pkg = ["null", "0", "0"]

            return {self.words[0]: "unknown", 
                    self.words[1]: info_pkg[0], 
                    self.words[2]: info_pkg[1], 
                    self.words[3]: "unknown", 
                    self.words[4]: ""}

    def install(self, lpkg):
        """Install LPKG file"""
        Log().info(f"Installing {lpkg}")
        if not options.debug:
            subprocess.getoutput(sh+" install "+lpkg)
        else:
            subprocess.run([sh,"install",lpkg])

    def verify(self, name):
        """Check if it's installed"""
        installed = subprocess.getoutput(sh+" list "+name)
        if name in installed:
            Log().info(f"{name}: installed")
            return True
        else:
            Log().info(f"{name}: not installed")
            return False

    def remove(self, lpkg):
        """Remove LPKG file """
        Log().info(f"Uninstalling {lpkg}")
        if not options.debug:
            subprocess.getoutput(sh+" remove "+lpkg)
        else:
            subprocess.run([sh,"remove",lpkg])

    def get_image(self, pkg):
        """Get an image of LPKG if it's exists"""
        image = subprocess.getoutput(sh+' image '+pkg)
        if image != "null":
            Log().debug("Showing an image")
            return str(image)
        else:
            Log().debug("Showing lpkg icon")
            return str(self.icon_lpkg)

    def ifindex(self, link):
        """Verify that the lpkgindex page exists"""
        curl = 'curl --write-out "%{http_code}\n"'
        quiet = ' --silent --output /dev/null'
        if not options.verbose:
            curl = curl + quiet
        timeout=options.timeout
        status = subprocess.getoutput(curl+' --connect-timeout '+timeout+' "'+link+'"')
        if status == "200":
            Log().debug("lpkgindex: True")
            return True
        else:
            Log().debug("lpkgindex: False")
            return False

class Log():
    def __init__(self):
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        self.console_handler = logging.StreamHandler()
        if not options.verbose:
            self.console_handler.setLevel(logging.WARNING)
        self.fmt = {10:'\033[1;34mDEBUG:\033[0m',
                    20:'\033[1;32mINFO:\033[0m',
                    30:'\033[1;33mWARNING:\033[0m',
                    40:'\033[1;31mERROR:\033[0m',
                    50:'\033[1;31mCRITICAL:\033[0m'}
        self.formatter = logging.Formatter('%(message)s')
        self.console_handler.setFormatter(self.formatter)
        if len(self.logger.handlers) == 0:
            self.logger.addHandler(self.console_handler)
    def debug(self, msg):
        self.logger.log(10, f"{self.fmt[10]} {msg}")
    def info(self, msg):
        self.logger.log(20, f"{self.fmt[20]} {msg}")
    def warning(self, msg):
        self.logger.log(30, f"{self.fmt[30]} {msg}")
    def error(self, msg):
        self.logger.log(40, f"{self.fmt[40]} {msg}")
    def critical(self, msg):
        self.logger.log(50, f"{self.fmt[50]} {msg}")
if len(lpkg) > 0:
    if "/" not in lpkg[0] or "./" in lpkg[0]:
        Log().error(f"Relative path detected. Use \"$PWD/{lpkg[0]}\" or the complete path instead")
        os.sys.exit(1)
