# Glpkg

A graphic LPKG installer

## Dependencies

- [`lpkg`](https://gitlab.com/loc-os_linux/updates/-/blob/main/LOC-OS-LPKG-10.1-x86_64.Installer)

## Install

```bash
git clone https://gitlab.com/Anonyzard/glpkg.git
make
sudo make install
```

## Uninstall

```bash
sudo make uninstall
```

## Use

### From terminal

`pkexec glpkg </path/to/file.lpkg>`

### In "Open with" of PCManFM

`pkexec glpkg %f`

## Licence

All this proyect is licensed under GPLv3
