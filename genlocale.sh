#!/bin/bash
for locale in po/*; do
    polang="$(echo "$locale" | cut -d. -f1 | cut -d/ -f2)"
    outdir=locale/"$polang"/LC_MESSAGES
    ! [ -d "$outdir" ] && mkdir -p "$outdir"
    msgfmt -c "$locale" -o "$outdir"/glpkg.mo
done